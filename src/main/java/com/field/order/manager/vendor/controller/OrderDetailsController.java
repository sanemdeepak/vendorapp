package com.field.order.manager.vendor.controller;

import com.field.order.manager.vendor.domain.client.Order;
import com.field.order.manager.vendor.service.OrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

/**
 * Created by sanemdeepak on 12/8/18.
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/order")
public class OrderDetailsController {

    private final OrderService orderService;


    @GetMapping("/{id}")
    public Order getById(@PathVariable("id") UUID id) {
        return orderService.findById(id);
    }
}
