package com.field.order.manager.vendor.repo;

import com.field.order.manager.vendor.domain.person.Employee;
import com.field.order.manager.vendor.domain.person.EmployeeType;
import com.field.order.manager.vendor.domain.person.Skill;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

/**
 * Created by sanemdeepak on 12/3/18.
 */
@Repository
public interface EmployeeRepo extends MongoRepository<Employee, UUID>, EmployeeCustomRepo {

    Employee findByPhoneNumber(String phoneNumber);

    Employee findByNumber(Long number);

    List<Employee> findByLastName(String lastName);
}
