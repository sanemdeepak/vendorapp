package com.field.order.manager.vendor.repo.impl;

import com.field.order.manager.vendor.domain.assignment.OrderAssignment;
import com.field.order.manager.vendor.exception.ResourceNotFoundException;
import com.field.order.manager.vendor.repo.OrderAssignmentCustomRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

/**
 * Created by sanemdeepak on 12/3/18.
 */
@Repository
@RequiredArgsConstructor
public class OrderAssignmentCustomRepoImpl implements OrderAssignmentCustomRepo {

    private final MongoTemplate mongoTemplate;

    @Override
    public OrderAssignment update(UUID id, OrderAssignment orderAssignment) {
        Query query = Query.query(Criteria.where("id").is(id));

        OrderAssignment assignment = Optional.ofNullable(mongoTemplate.findOne(query, OrderAssignment.class))
                .orElseThrow(() -> new ResourceNotFoundException(String.format("No assignment with id: %s", id.toString())));

        assignment.setStatus(orderAssignment.getStatus());

        Update update = new Update()
                .set("assignmentNumber", assignment.getAssignmentNumber())
                .set("orderRef", assignment.getOrderRef())
                .set("employeeRef", orderAssignment.getEmployeeRef())
                .set("status", orderAssignment.getStatus());
        mongoTemplate.updateFirst(query, update, OrderAssignment.class);

        return mongoTemplate.findOne(query, OrderAssignment.class);
    }
}
