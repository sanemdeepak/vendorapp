package com.field.order.manager.vendor.service.impl;

import com.field.order.manager.vendor.domain.client.Order;
import com.field.order.manager.vendor.repo.OrderRepo;
import com.field.order.manager.vendor.service.OrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.UUID;

/**
 * Created by sanemdeepak on 12/8/18.
 */
@Service
@RequiredArgsConstructor
public class OrderServiceImpl implements OrderService {

    private final OrderRepo orderRepo;

    @Override
    public Order findById(UUID id) {
        return orderRepo.findById(id);
    }
}
