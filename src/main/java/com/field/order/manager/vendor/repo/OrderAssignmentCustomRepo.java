package com.field.order.manager.vendor.repo;

import com.field.order.manager.vendor.domain.assignment.OrderAssignment;

import java.util.UUID;

/**
 * Created by sanemdeepak on 12/3/18.
 */
public interface OrderAssignmentCustomRepo {
    OrderAssignment update(UUID id, OrderAssignment orderAssignment);
}
