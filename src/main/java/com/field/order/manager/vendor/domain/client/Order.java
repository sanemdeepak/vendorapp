package com.field.order.manager.vendor.domain.client;

import com.field.order.manager.vendor.domain.address.Address;
import com.field.order.manager.vendor.domain.customer.CustomerRef;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.UUID;

/**
 * Created by sanemdeepak on 11/15/18.
 */

@Data
public class Order {

    @Id
    private UUID id;

    @Indexed(unique = true)
    private Long number;

    @NotNull
    @Valid
    private CustomerRef forCustomer;

    @NotNull
    @Valid
    private ServiceType serviceType;

    @NotNull
    @Valid
    private Address addressForOrder;

    private String serviceDesc;

    private String additionalContactName;

    private String additionalContactNumber;
}
