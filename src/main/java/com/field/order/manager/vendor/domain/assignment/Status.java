package com.field.order.manager.vendor.domain.assignment;

/**
 * Created by sanemdeepak on 12/3/18.
 */
public enum  Status {
    PENDING,
    ACTIVE,
    CLOSED
}
