package com.field.order.manager.vendor.repo;

import com.field.order.manager.vendor.domain.assignment.OrderAssignment;
import com.field.order.manager.vendor.domain.client.OrderRef;
import com.field.order.manager.vendor.domain.person.EmployeeRef;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

/**
 * Created by sanemdeepak on 12/3/18.
 */
@Repository
public interface OrderAssignmentRepo extends MongoRepository<OrderAssignment, UUID>, OrderAssignmentCustomRepo {

    OrderAssignment findByAssignmentNumber(Long number);

    List<OrderAssignment> findByOrderRef(OrderRef orderRef);

    List<OrderAssignment> findAllByEmployeeRef(EmployeeRef employeeRef);

}
