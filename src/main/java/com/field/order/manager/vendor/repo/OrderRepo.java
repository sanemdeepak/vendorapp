package com.field.order.manager.vendor.repo;

import com.field.order.manager.vendor.domain.client.Order;

import java.util.UUID;

/**
 * Created by sanemdeepak on 12/8/18.
 */
public interface OrderRepo {
    Order findById(UUID id);
}
