package com.field.order.manager.vendor.controller;

import com.field.order.manager.vendor.domain.person.Employee;
import com.field.order.manager.vendor.domain.person.EmployeeType;
import com.field.order.manager.vendor.domain.person.Skill;
import com.field.order.manager.vendor.exception.IllFormedDataException;
import com.field.order.manager.vendor.service.EmployeeService;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by sanemdeepak on 12/3/18.
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/employee")
public class EmployeeController {

    private final EmployeeService employeeService;


    @PostMapping
    public Employee create(@RequestBody @Valid Employee employee) {
        return employeeService.create(employee);
    }

    @GetMapping("/{employeeType}")
    public List<Employee> getAllBySkills(@RequestParam MultiValueMap<String, String> params, @PathVariable("employeeType") EmployeeType employeeType) {

        String skillParamsAsString = params.getFirst("skills");
        List<Skill> skills = getSkillsFromStringArray(skillParamsAsString);
        return employeeService.findAllBySkillsAAndEmployeeType(skills, employeeType);

    }

    private List<Skill> getSkillsFromStringArray(String input) {
        String[] strings = StringUtils.split(input, ",");

        return Stream.of(strings).distinct().map(val -> {
            try {
                return Skill.valueOf(val.toUpperCase());
            } catch (IllegalArgumentException | NullPointerException exp) {
                throw new IllFormedDataException(String.format("Invalid skill, %s", val));
            }
        }).collect(Collectors.toList());
    }
}
