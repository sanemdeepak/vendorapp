package com.field.order.manager.vendor.domain.client;

import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created by sanemdeepak on 12/3/18.
 */
@Data
public class ClientOrder {
    @Valid
    @NotNull
    private OrderRef orderRef;
}
