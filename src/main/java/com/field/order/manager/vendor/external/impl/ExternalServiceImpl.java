package com.field.order.manager.vendor.external.impl;

import com.field.order.manager.vendor.domain.assignment.OrderAssignment;
import com.field.order.manager.vendor.domain.assignment.Status;
import com.field.order.manager.vendor.domain.client.ClientOrder;
import com.field.order.manager.vendor.domain.person.Employee;
import com.field.order.manager.vendor.domain.person.EmployeeRef;
import com.field.order.manager.vendor.external.ExternalService;
import com.field.order.manager.vendor.notification.email.EmailSender;
import com.field.order.manager.vendor.service.EmployeeService;
import com.field.order.manager.vendor.service.OrderAssignmentService;
import com.field.order.manager.vendor.util.Util;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * Created by sanemdeepak on 12/3/18.
 */
@Service
@RequiredArgsConstructor
public class ExternalServiceImpl implements ExternalService {

    private final OrderAssignmentService assignmentService;
    private final EmployeeService employeeService;
    private final EmailSender emailSender;
    private final Util util;

    @Override
    public Long process(ClientOrder clientOrder) {

        OrderAssignment newAssignment = new OrderAssignment();
        newAssignment.setStatus(Status.PENDING);
        newAssignment.setOrderRef(clientOrder.getOrderRef());

        Employee employee = employeeService.findRandomManager();
        EmployeeRef employeeRef = new EmployeeRef();
        employeeRef.setId(employee.getId());

        newAssignment.setEmployeeRef(employeeRef);

        newAssignment = assignmentService.assign(newAssignment);

        emailSender.sendEmailNotification(util.notificationMessage(newAssignment.getAssignmentNumber()), employee.getEmail(), employee.getFirstName());

        return newAssignment.getAssignmentNumber();
    }
}
