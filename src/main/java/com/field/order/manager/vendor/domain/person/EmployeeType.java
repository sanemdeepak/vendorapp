package com.field.order.manager.vendor.domain.person;

/**
 * Created by sanemdeepak on 12/3/18.
 */
public enum EmployeeType {
    SUPERVISOR,
    TECHNICIAN
}
