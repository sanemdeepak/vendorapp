package com.field.order.manager.vendor.service;

import com.field.order.manager.vendor.domain.person.Employee;
import com.field.order.manager.vendor.domain.assignment.OrderAssignment;
import org.springframework.stereotype.Component;

import java.security.SecureRandom;
import java.util.UUID;

/**
 * Created by sanemdeepak on 12/3/18.
 */
@Component
public class PreProcessor {

    private final SecureRandom secureRandom;

    public PreProcessor() {
        try {
            this.secureRandom = SecureRandom.getInstance("SHA1PRNG");
        } catch (Exception exp) {
            throw new RuntimeException(exp);
        }
    }

    public Employee processEmployee(Employee employee) {

        employee.setId(UUID.randomUUID());
        employee.setNumber(getRandomLong());

        return employee;
    }

    private Long getRandomLong() {
        return (long) (10000 + secureRandom.nextInt(90000));
    }

    public OrderAssignment processOrderAssignment(OrderAssignment assignment) {
        assignment.setId(UUID.randomUUID());
        assignment.setAssignmentNumber(getRandomLong());
        return assignment;
    }
}
