package com.field.order.manager.vendor.external;

import com.field.order.manager.vendor.domain.client.ClientOrder;

/**
 * Created by sanemdeepak on 12/3/18.
 */
public interface ExternalService {

    Long process(ClientOrder clientOrder);
}
