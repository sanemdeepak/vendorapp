package com.field.order.manager.vendor.external;

import com.field.order.manager.vendor.domain.client.ClientOrder;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * Created by sanemdeepak on 12/3/18.
 */
@RestController
@RequiredArgsConstructor
@Slf4j
@RequestMapping("external")
public class ServiceController {

    private final ExternalService externalService;


    @PostMapping
    public Long processOrder(@RequestBody @Valid ClientOrder clientOrder) {
        return externalService.process(clientOrder);
    }
}
