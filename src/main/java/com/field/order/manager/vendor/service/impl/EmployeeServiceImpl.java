package com.field.order.manager.vendor.service.impl;

import com.field.order.manager.vendor.domain.person.Employee;
import com.field.order.manager.vendor.domain.person.EmployeeType;
import com.field.order.manager.vendor.domain.person.Skill;
import com.field.order.manager.vendor.exception.ResourceNotFoundException;
import com.field.order.manager.vendor.repo.EmployeeRepo;
import com.field.order.manager.vendor.service.EmployeeService;
import com.field.order.manager.vendor.service.PreProcessor;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

/**
 * Created by sanemdeepak on 12/3/18.
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class EmployeeServiceImpl implements EmployeeService {

    private final EmployeeRepo employeeRepo;
    private final PreProcessor preProcessor;


    @Override
    public List<Employee> findAllBySkillsAAndEmployeeType(List<Skill> skills, EmployeeType employeeType) {
        return employeeRepo.findAllBySkillsAAndEmployeeType(skills, employeeType);
    }

    @Override
    public Employee findById(UUID id) {
        return employeeRepo.findById(id).orElseThrow(() -> new ResourceNotFoundException(String.format("No Employee with id: %s", id.toString())));
    }

    @Override
    public Employee findByNumber(Long employeeNumber) {
        return employeeRepo.findByNumber(employeeNumber);
    }

    @Override
    public Employee findByPhoneNumber(String phoneNumber) {
        return employeeRepo.findByPhoneNumber(phoneNumber);
    }

    @Override
    public List<Employee> findAllByLastName(String lastName) {
        return employeeRepo.findByLastName(lastName);
    }

    @Override
    public Employee create(Employee employee) {
        employee = preProcessor.processEmployee(employee);
        return employeeRepo.insert(employee);
    }

    @Override
    public Employee update(UUID id, Employee employee) {
        return null;
    }

    @Override
    public Employee findRandomManager() {
        return employeeRepo.findAllManagers().stream().findAny().orElseThrow(ResourceNotFoundException::new);
    }
}
