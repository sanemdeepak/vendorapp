package com.field.order.manager.vendor.controller;

import com.field.order.manager.vendor.domain.assignment.OrderAssignment;
import com.field.order.manager.vendor.domain.assignment.Status;
import com.field.order.manager.vendor.service.OrderAssignmentService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

/**
 * Created by sanemdeepak on 12/3/18.
 */
@RestController
@RequestMapping("/assignment")
@RequiredArgsConstructor
@Slf4j
public class AssignmentController {

    private final OrderAssignmentService orderAssignmentService;

    @PostMapping
    public OrderAssignment assign(@RequestBody @Valid OrderAssignment assignment) {
        return orderAssignmentService.assign(assignment);
    }

    @GetMapping("/orderAssignmentId/{orderAssignmentId}")
    public OrderAssignment getById(@PathVariable("orderAssignmentId") UUID id) {
        return orderAssignmentService.findById(id);
    }

    @GetMapping("/assignmentNumber/{assignmentNumber}")
    public OrderAssignment getByNumber(@PathVariable("assignmentNumber") Long number) {
        return orderAssignmentService.findByNumber(number);
    }

    @GetMapping("/orderId/{orderId}")
    public List<OrderAssignment> getAllByOrderId(@PathVariable("orderId") UUID orderId) {
        return orderAssignmentService.findAllForOrderId(orderId);
    }

    @GetMapping("/employeeId/{employeeId}")
    public List<OrderAssignment> getAllByEmployeeId(@PathVariable("employeeId") UUID employeeId) {
        return orderAssignmentService.findAllForEmployeeId(employeeId);
    }

    @PutMapping("/{orderAssignmentId}")
    public OrderAssignment update(@PathVariable("orderAssignmentId") UUID id, @RequestBody OrderAssignment orderAssignment) {
        return orderAssignmentService.updateOrderAssignment(id, orderAssignment);
    }

    @PutMapping("/{orderAssignmentId}/status/{status}")
    public OrderAssignment update(@PathVariable("orderAssignmentId") UUID id, @PathVariable("status") Status status) {
        return orderAssignmentService.updateOrderAssignmentStatus(id, status);
    }
}
