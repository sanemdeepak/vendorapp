package com.field.order.manager.vendor.service.impl;

import com.field.order.manager.vendor.domain.assignment.OrderAssignment;
import com.field.order.manager.vendor.domain.assignment.Status;
import com.field.order.manager.vendor.domain.client.OrderRef;
import com.field.order.manager.vendor.domain.person.Employee;
import com.field.order.manager.vendor.domain.person.EmployeeRef;
import com.field.order.manager.vendor.domain.person.EmployeeType;
import com.field.order.manager.vendor.exception.ResourceNotFoundException;
import com.field.order.manager.vendor.notification.email.EmailSender;
import com.field.order.manager.vendor.repo.OrderAssignmentRepo;
import com.field.order.manager.vendor.service.EmployeeService;
import com.field.order.manager.vendor.service.OrderAssignmentService;
import com.field.order.manager.vendor.service.PreProcessor;
import com.field.order.manager.vendor.util.Util;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

/**
 * Created by sanemdeepak on 12/3/18.
 */
@Service
@RequiredArgsConstructor
public class OrderAssignmentServiceImpl implements OrderAssignmentService {
    private final OrderAssignmentRepo orderAssignmentRepo;
    private final EmployeeService employeeService;
    private final EmailSender emailSender;
    private final PreProcessor preProcessor;
    private final Util util;

    @Override
    public OrderAssignment assign(OrderAssignment assignment) {
        assignment = preProcessor.processOrderAssignment(assignment);
        if (assignment.getStatus() == null) {
            assignment.setStatus(Status.PENDING);
        }
        return orderAssignmentRepo.insert(assignment);
    }

    @Override
    public OrderAssignment findById(UUID id) {
        return orderAssignmentRepo.findById(id).orElseThrow(() -> new ResourceNotFoundException("No order assignment found"));
    }

    @Override
    public OrderAssignment findByNumber(Long number) {
        return orderAssignmentRepo.findByAssignmentNumber(number);
    }

    @Override
    public List<OrderAssignment> findAllForOrderId(UUID id) {
        OrderRef ref = new OrderRef();
        ref.setId(id);
        return orderAssignmentRepo.findByOrderRef(ref);
    }

    @Override
    public List<OrderAssignment> findAllForEmployeeId(UUID id) {
        EmployeeRef ref = new EmployeeRef();
        ref.setId(id);
        return orderAssignmentRepo.findAllByEmployeeRef(ref);
    }

    @Override
    public OrderAssignment updateOrderAssignment(UUID id, OrderAssignment orderAssignment) {
        OrderAssignment existingAssignment = findById(id);

        if (existingAssignment.getEmployeeRef().equals(orderAssignment.getEmployeeRef())) {
            return orderAssignment;
        }

        Employee assignedToEmployee = employeeService.findById(orderAssignment.getEmployeeRef().getId());

        if (assignedToEmployee.getEmployeeType().equals(EmployeeType.TECHNICIAN)) {
            orderAssignment.setStatus(Status.ACTIVE);
        } else {
            orderAssignment.setStatus(Status.PENDING);
        }
        orderAssignment = orderAssignmentRepo.update(id, orderAssignment);
        emailSender.sendEmailNotification(util.notificationMessage(orderAssignment.getAssignmentNumber()), assignedToEmployee.getEmail(), assignedToEmployee.getFirstName());
        return orderAssignment;
    }

    @Override
    public OrderAssignment updateOrderAssignmentStatus(UUID id, Status status) {
        OrderAssignment assignment = this.findById(id);
        assignment.setStatus(status);
        return updateOrderAssignment(id, assignment);
    }
}
