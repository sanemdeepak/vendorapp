package com.field.order.manager.vendor.repo.impl;

import com.field.order.manager.vendor.domain.client.Order;
import com.field.order.manager.vendor.repo.OrderRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import java.util.UUID;

/**
 * Created by sanemdeepak on 12/8/18.
 */
@Repository
@RequiredArgsConstructor
public class OrderRepoImpl implements OrderRepo {

    @Value("${field.order.manager.url}")
    private String fieldOrderManagerUrl;

    private final RestTemplate restTemplate;


    @Override
    public Order findById(UUID id) {
        return restTemplate.getForObject(String.format("%s/%s/%s", fieldOrderManagerUrl, "order", id.toString()), Order.class);
    }
}
