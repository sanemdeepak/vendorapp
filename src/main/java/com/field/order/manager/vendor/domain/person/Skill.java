package com.field.order.manager.vendor.domain.person;

/**
 * Created by sanemdeepak on 12/3/18.
 */
public enum Skill {
    ELECTRICAL,
    AC,
    AC_COOLENT,
    AC_DUCTING,
    HVAC,
    PLUMBING,
    CIVIL;
}
