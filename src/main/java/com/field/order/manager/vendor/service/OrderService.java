package com.field.order.manager.vendor.service;

import com.field.order.manager.vendor.domain.client.Order;

import java.util.UUID;

/**
 * Created by sanemdeepak on 12/8/18.
 */
public interface OrderService {

    Order findById(UUID id);
}
