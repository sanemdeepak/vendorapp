package com.field.order.manager.vendor.domain.person;

import com.field.order.manager.vendor.domain.address.Address;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.UUID;

/**
 * Created by sanemdeepak on 12/3/18.
 */
@Data
@Document(collection = "Employee")
public class Employee {
    @Id
    private UUID id;

    @Indexed(unique = true)
    private Long number;
    @NotBlank
    private String firstName;
    @NotBlank
    private String lastName;
    @Valid
    private Address address;
    @Indexed(unique = true)
    @NotBlank
    private String phoneNumber;
    @Indexed(unique = true)
    @NotBlank
    private String email;

    @Indexed
    private List<Skill> skills;

    @NotNull
    private EmployeeType employeeType;
}
