package com.field.order.manager.vendor.domain.client;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.UUID;

/**
 * Created by sanemdeepak on 12/3/18.
 */
@Data
public class OrderRef {
    @NotNull
    private UUID id;
}
