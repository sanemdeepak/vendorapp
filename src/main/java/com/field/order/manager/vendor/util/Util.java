package com.field.order.manager.vendor.util;

import org.springframework.stereotype.Component;

/**
 * Created by sanemdeepak on 12/6/18.
 */
@Component
public class Util {

    public String notificationMessage(Long assignmentNumber) {
        return String.format("You have been assigned an order, reference number: %d", assignmentNumber);
    }
}
