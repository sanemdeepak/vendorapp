package com.field.order.manager.vendor.repo;

import com.field.order.manager.vendor.domain.person.Employee;
import com.field.order.manager.vendor.domain.person.EmployeeType;
import com.field.order.manager.vendor.domain.person.Skill;

import java.util.List;

/**
 * Created by sanemdeepak on 12/5/18.
 */
public interface EmployeeCustomRepo {
    List<Employee> findAllBySkillsAAndEmployeeType(List<Skill> skills, EmployeeType employeeType);

    List<Employee> findAllManagers();
}
