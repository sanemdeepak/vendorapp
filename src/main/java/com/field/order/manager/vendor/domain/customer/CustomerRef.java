package com.field.order.manager.vendor.domain.customer;

import lombok.Data;

import java.util.UUID;

/**
 * Created by sanemdeepak on 11/28/18.
 */
@Data
public class CustomerRef {
    private UUID id;
}
