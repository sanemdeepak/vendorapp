package com.field.order.manager.vendor.service;

import com.field.order.manager.vendor.domain.person.Employee;
import com.field.order.manager.vendor.domain.person.EmployeeType;
import com.field.order.manager.vendor.domain.person.Skill;

import java.util.List;
import java.util.UUID;

/**
 * Created by sanemdeepak on 12/3/18.
 */
public interface EmployeeService {

    List<Employee> findAllBySkillsAAndEmployeeType(List<Skill> skills, EmployeeType employeeType);

    Employee findById(UUID id);

    Employee findByNumber(Long employeeNumber);

    Employee findByPhoneNumber(String phoneNumber);

    List<Employee> findAllByLastName(String lastName);

    Employee create(Employee employee);

    Employee update(UUID id, Employee employee);

    Employee findRandomManager();

}
