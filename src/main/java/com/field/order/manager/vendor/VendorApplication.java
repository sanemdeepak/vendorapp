package com.field.order.manager.vendor;

import com.field.order.manager.vendor.exception.HttpClientErrorHandler;
import com.mailjet.client.ClientOptions;
import com.mailjet.client.MailjetClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class VendorApplication {


    @Value("${mailjet.public.key}")
    private String apiPublicKey;

    @Value("${mailjet.secret.key}")
    private String apiSecretKey;

    public static void main(String[] args) {
        SpringApplication.run(VendorApplication.class, args);
    }

    @Bean
    public MailjetClient getMailjetClient() {
        return new MailjetClient(apiPublicKey, apiSecretKey, new ClientOptions("v3.1"));
    }


    @Bean
    public RestTemplate getRestTemplate() {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.setErrorHandler(new HttpClientErrorHandler());
        return restTemplate;
    }
}
