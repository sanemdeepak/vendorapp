package com.field.order.manager.vendor.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by sanemdeepak on 12/3/18.
 */
@ResponseStatus(HttpStatus.BAD_REQUEST)
public class IllFormedDataException extends RuntimeException {

    public IllFormedDataException(String message) {
        super(message);
    }
}
