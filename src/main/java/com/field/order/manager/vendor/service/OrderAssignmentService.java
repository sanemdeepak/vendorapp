package com.field.order.manager.vendor.service;

import com.field.order.manager.vendor.domain.assignment.OrderAssignment;
import com.field.order.manager.vendor.domain.assignment.Status;

import java.util.List;
import java.util.UUID;

/**
 * Created by sanemdeepak on 12/3/18.
 */
public interface OrderAssignmentService {

    OrderAssignment assign(OrderAssignment assignment);

    OrderAssignment findById(UUID id);

    OrderAssignment findByNumber(Long number);

    List<OrderAssignment> findAllForOrderId(UUID id);

    List<OrderAssignment> findAllForEmployeeId(UUID id);

    OrderAssignment updateOrderAssignment(UUID id, OrderAssignment orderAssignment);

    OrderAssignment updateOrderAssignmentStatus(UUID id, Status status);

}
