package com.field.order.manager.vendor.notification.email;

import com.mailjet.client.MailjetClient;
import com.mailjet.client.MailjetRequest;
import com.mailjet.client.MailjetResponse;
import com.mailjet.client.errors.MailjetException;
import com.mailjet.client.errors.MailjetSocketTimeoutException;
import com.mailjet.client.resource.Emailv31;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.stereotype.Component;

/**
 * Created by sanemdeepak on 12/1/18.
 */
@Component
@RequiredArgsConstructor
@Slf4j
public class EmailSender {

    private final MailjetClient mailjetClient;


    public void sendEmailNotification(String body, String email, String name) {

        EmailData data = new EmailData();
        data.setToEmailAddress(email);
        data.setToName(name);
        data.setBody(body);
        MailjetRequest mailjetRequest = new MailjetRequest(Emailv31.resource).property(Emailv31.MESSAGES, data.getJsonArray());
        try {
            MailjetResponse mailjetResponse = mailjetClient.post(mailjetRequest);
            if (mailjetResponse.getStatus() != 200) {
                log.error(String.format("Error sending email to: %s, reason:%s", email, mailjetResponse.getData()));
            }
        } catch (MailjetException | MailjetSocketTimeoutException e) {
            log.error("Exception while sending email reason: {}", ExceptionUtils.getStackTrace(e));
            throw new RuntimeException(e);
        }
        log.info(String.format("Email sent to: %s message: %s", email, body));
    }
}
