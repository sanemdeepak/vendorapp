package com.field.order.manager.vendor.repo.impl;

import com.field.order.manager.vendor.domain.person.Employee;
import com.field.order.manager.vendor.domain.person.EmployeeType;
import com.field.order.manager.vendor.domain.person.Skill;
import com.field.order.manager.vendor.repo.EmployeeCustomRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by sanemdeepak on 12/5/18.
 */
@Repository
@RequiredArgsConstructor
public class EmployeeCustomRepoImpl implements EmployeeCustomRepo {

    private final MongoTemplate mongoTemplate;

    @Override
    public List<Employee> findAllBySkillsAAndEmployeeType(List<Skill> skills, EmployeeType employeeType) {
        Criteria criteria = Criteria.where("employeeType").is(employeeType).and("skills").all(skills);

        return mongoTemplate.find(Query.query(criteria), Employee.class);
    }

    @Override
    public List<Employee> findAllManagers() {
        Query query = Query.query(Criteria.where("employeeType").is(EmployeeType.SUPERVISOR));
        return mongoTemplate.find(query, Employee.class);
    }
}
