package com.field.order.manager.vendor.domain.assignment;

import com.field.order.manager.vendor.domain.client.OrderRef;
import com.field.order.manager.vendor.domain.person.EmployeeRef;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.UUID;

/**
 * Created by sanemdeepak on 12/3/18.
 */
@Data
@Document(collection = "OrderAssignment")
public class OrderAssignment {

    @Id
    private UUID id;

    @Indexed(unique = true)
    private Long assignmentNumber;


    //TODO review uniqueness
    @NotNull
    @Valid
    private OrderRef orderRef;

    @NotNull
    @Valid
    private EmployeeRef employeeRef;

    private Status status;

}
