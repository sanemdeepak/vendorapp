package com.field.order.manager.vendor.domain.person;

import lombok.Data;

import java.util.UUID;

/**
 * Created by sanemdeepak on 12/3/18.
 */
@Data
public class EmployeeRef {
    private UUID id;
}
