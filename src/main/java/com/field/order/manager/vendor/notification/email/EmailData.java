package com.field.order.manager.vendor.notification.email;

import com.mailjet.client.resource.Emailv31;
import lombok.Data;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by sanemdeepak on 12/1/18.
 */
@Data
public class EmailData {
    private String toEmailAddress;
    private String toName;
    private String body;


    public JSONArray getJsonArray() {
        return new JSONArray()
                .put(new JSONObject()
                        .put(Emailv31.Message.FROM, new JSONObject()
                                .put("Email", "sanemdeepak@mail.com")
                                .put("Name", "EFSR-INFO"))
                        .put(Emailv31.Message.TO, new JSONArray()
                                .put(new JSONObject()
                                        .put("Email", toEmailAddress)
                                        .put("Name", toName)))
                        .put(Emailv31.Message.SUBJECT, "New Order Assigned")
                        .put(Emailv31.Message.TEXTPART, body));

    }
}
